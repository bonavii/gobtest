package main

import (
	"encoding/gob"
	"fmt"
	"git.yabbi.me/modules/extrarpc"
	"gitlab.com/bonavii/gobtest/stype"
)

type Events struct{}

func main() {
	gob.Register(stype.Testing{})

	address := "localhost:8061"
	fmt.Println("listening " + address)

	rpcServer := extrarpc.NewServer(address)

	err := rpcServer.Register(&Events{})
	if err != nil {
		fmt.Println(err)
	}

	err = rpcServer.Run()
	if err != nil {
		fmt.Println(err)
	}
}

func (a *Events) Send(req stype.Testing, resp *struct{}) error {

	fmt.Println(req)

	return nil

}
